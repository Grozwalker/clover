</div>
<div class="push"></div>
</div>
</div>
<footer>
    <div class="footer-contact-info">
        <p>г. Краснодар, ул. Академическая, дом 24</p>
        <p>тел. 8 918 123-12-12</p>
    </div>
    <nav class="footer-navigation">
        <? wp_nav_menu(array('menu' => 'Bottom-menu', 'menu_class' => 'footer-menu')); ?>
    </nav>
    <div class="clear"></div>
    <p class="copy">&copy; ЧДС «Клевер», 2015 г.</p>

</footer>
<?php wp_footer(); ?>
</body>
</html>