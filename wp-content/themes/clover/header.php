<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">	
    <meta name="viewport" content="width=960">
    <title><?php wp_title('«', true, 'right'); ?></title>
    <link rel="pingback" href="clever/" />
    <?php wp_head(); ?>
    <!-- Подключаем стили-->
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/clover/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/clover/css/style.css">
    <!-- Конец: Подключаем стили-->
	
<!-- Add jQuery library -->
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

	<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="/wp-content/themes/clover/include/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="/wp-content/themes/clover/include/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="/wp-content/themes/clover/include/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="/wp-content/themes/clover/include/fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="/wp-content/themes/clover/include/fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="/wp-content/themes/clover/include/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="/wp-content/themes/clover/include/fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="/wp-content/themes/clover/include/fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".gallery").fancybox();
	});
</script>

</head>
<body>
<div class="wrapper">
<header>
    <div class="header-content">
        <div class="header-content-bg">
            <a href="/"><img src="/wp-content/themes/clover/img/logo.png" alt = 'Частный детский сад: "Клевер"' title='Частный детский сад: "Клевер"' /></a>
            <span class="header-phone">тел. 8 918 123-12-12</span>
            <nav class="main-navigation">
                <? wp_nav_menu(array('menu' => 'top-menu')); ?>
            </nav>
            <span  class="header-adress">Краснодар, ул. Академическая, дом 24</span>
        </div>
    </div>
</header>
<div class="content-wrapper">
    <aside>
		<iframe src='/wp-content/plugins/inwidget/index.php?width=100&inline=2&view=12&toolbar=false' scrolling='no' frameborder='no' style='border:none;width:100px;height:320px;overflow:hidden;'></iframe>
    </aside>
	
	<div class = "content">
	
			<!--Вывод слайдера только на главной странице-->
		<?php
			if ((is_front_page()) and (!is_paged())) {
				echo '<div class="slider">';

				//Выводим слайдер только на главной странице
				if ( function_exists( 'meteor_slideshow' ) ) { meteor_slideshow(); }
				echo '</div>';
			} else {
				//<!--Вывод хлебных крошек-->
		 		if( function_exists('breadcrumbs') ) breadcrumbs();
			}
		?>