<?php
/* Вставка соцкнопок*/
add_action('comments_template','soc_button');
function soc_button() {?>

<style>
.share a {
display: inline-block;
vertical-align: inherit;
margin: 5px 0 0 2px;
padding: 0px;
font-size: 0px;
width: 40px;
height: 40px;
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll 0px 0px transparent;}

.share a.vkontakte {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -168px 0px transparent;
}
.share a.google {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -252px 0px transparent;
}
.share a.livejournal {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -336px 0px transparent;
}
.share a.twitter {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -42px 0px transparent;
}
.share a.mail {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -294px 0px transparent;
}
.share a.odnoklassniki {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -126px 0px transparent;
}
.share a.pinterest {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -210px 0px transparent;
}
.share a.liveinternet {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -378px 0px transparent;
}
.share a.evernote {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -420px 0px transparent;
}
.share a.bookmark {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -462px 0px transparent;
}
.share a.email {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -504px 0px transparent;
}
.share a.print {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -546px 0px transparent;
}
.share a.digg {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -588px 0px transparent;
}
.share a.spring {
background: url("http://test.1zaicev.ru/wp-content/uploads/2015/04/soc_icon.png") no-repeat scroll -630px 0px transparent;
}
</style>
<div class="share">
<a onClick="window.open('https://vkontakte.ru/share.php?url=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="vkontakte"></a>
<a onClick="window.open('https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="facebook"></a>
<a onClick="window.open('https://plus.google.com/share?url=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="google"></a>
<a onClick="window.open('http://www.livejournal.com/update.bml?event=<?php the_permalink(); ?>&subject=<?php the_title(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="livejournal"></a>
<a onClick="window.open('https://twitter.com/intent/tweet?text=<?php the_title(); ?> <?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="twitter"></a>
<a onClick="window.open('https://connect.mail.ru/share?url=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="mail"></a>
<a onClick="window.open('http://ok.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="odnoklassniki"></a>
<a onClick="window.open('https://ru.pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=700,height=400');" href="javascript: void(0)" class="pinterest"></a>
<a onClick="window.open('https://www.evernote.com/clip.action?url=<?php the_permalink(); ?>&target=blog','sharer','toolbar=0,status=0,width=930,height=500');" href="javascript: void(0)" class="evernote"></a>
<a onClick="window.open('http://www.liveinternet.ru/journal_post.php?action=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=812,height=585');" href="javascript: void(0)" class="liveinternet"></a>
<a onClick="window.open('http://digg.com/submit?url=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=812,height=585');" href="javascript: void(0)" class="digg"></a>
<a onClick="window.open('http://new.spring.me/#!/welcome?url=<?php the_permalink(); ?>','sharer','toolbar=0,status=0,width=812,height=585');" href="javascript: void(0)" class="spring"></a>
<a href="<?php the_permalink(); ?>" class="bookmark" rel="sidebar"></a>
<a href="mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>" class="email"></a>
<a href='javascript:window.print(); void 0;' class="print"></a>
</div>
<?php
}