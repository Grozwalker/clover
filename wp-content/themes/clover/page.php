<?php 

get_header();

	if (have_posts()): while (have_posts()): the_post(); 
		 the_content(); 
	endwhile; endif;


	if (is_page( 'about' )) {


		$post = get_page_by_path( 'certificates' );
		$content = apply_filters('the_content', $post->post_content);
		echo get_the_title( $post );
		echo '<div class="about-certificates">' . $content . '</div>';


		$page = get_page_by_path( 'license' );
		$page_content = apply_filters('the_content', $page->post_content);
		echo get_the_title( $page );
		echo '<div class="about-license">' . $page_content . '</div>';

	}


get_footer(); 

?>