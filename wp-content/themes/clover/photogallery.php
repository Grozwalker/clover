<?php
/* Template Name: Галерея */
?>

<?php 

get_header();



	
				//На странице Преподавателей выводим всех преподов из категории списком
			//	Если перешли на конкретного препода - выводим только его
			if (is_page( 'photo-albums' )) {

				$mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc' ) );

				foreach( $mypages as $page ) {      
					$content = $page->post_content;
					if ( ! $content ) // Check for empty page
						continue;

					$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($page->ID), 'medium' ); // возвращает массив параметров миниатюры
					
					$link = get_page_link($page->ID);

					echo ' <a href="' . get_page_link( $page->ID ) . '"><img src="'.$thumbnail_attributes[0] .'" />'; // URL миниатюры 
					
					
			
				} 
			} else {
				echo wp_title('');
				if (have_posts()): while (have_posts()): the_post(); 
				
					 the_content(); 
				endwhile; endif; 
			}
		?>
    




<?php
get_footer(); 

?>