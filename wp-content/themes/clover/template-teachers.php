<?php
/* Template Name: Преподаватели */
?>

<?php get_header(); ?>
		
		<?php

			//На странице Преподавателей выводим всех преподов из категории списком
			//	Если перешли на конкретного препода - выводим только его
			if (is_page( 'teachers' )) {
			

				$mypages = get_pages( array( 'parent' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc', 'hierarchical' => 0 ) );

				foreach( $mypages as $page ) {      
					$content = $page->post_content;
					if ( ! $content ) // Check for empty page
						continue;

					$content = apply_filters( 'the_content', $content );
		?>	
					<h2><a href="<?php echo get_page_link( $page->ID ); ?>"><?php echo $page->post_title; ?></a></h2>
					
		<?php 
					$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($page->ID), 'medium' ); // возвращает массив параметров миниатюры

					echo '<div class = "img_preview"> <img src="'.$thumbnail_attributes[0] .'" /></div>'; // URL миниатюры 
					
					echo $content;
					
					
					$mylink = get_pages( array( 'parent' => $page->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc', 'hierarchical' => 0 ) );
					
					foreach( $mylink as $link ) {      
					$currentlink = $link->post_content;
					if ( ! $currentlink ) // Check for empty page
						continue;
						
							echo '<a href="' . get_page_link( $link->ID ) . '">' . $link -> post_title . '</a><br />';
					}
					
				} 
			} else {
				
				
			
				if (have_posts()): while (have_posts()): the_post();

					$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); // возвращает массив параметров миниатюры

					echo '<div class = "img_preview"> <img src="'.$thumbnail_attributes[0] .'" /></div>'; // URL миниатюры

					the_content();

				 endwhile; endif;
				 
				 $mypages = get_pages( array( 'child_of' => $post->ID, 'sort_column' => 'post_date', 'sort_order' => 'desc' ) );

				 foreach( $mypages as $page ) {

				 $content = $page->post_content;
					if ( ! $content ) // Check for empty page
						continue;

					echo '<a href="' . get_page_link( $page->ID ) . '">' . $page->post_title . '</a>';
				 
				 }
			}
		?>


    

<?php get_footer(); ?>