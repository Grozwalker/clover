<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 18.02.2016
 * Time: 23:15
 * Template Name: Статьи
 **/

?>

<?php get_header(); ?>

<?php

if (have_posts()): while (have_posts()): the_post();

    wp_title('');
    the_time('j F Y');

    $thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); // возвращает массив параметров миниатюры

    echo '<div class = "img_preview"> <img src="'.$thumbnail_attributes[0] .'" /></div>'; // URL миниатюры

    the_content();

endwhile; endif;

?>

<style>
/*SOCIAL ICONS*/
.social_vertical {position:fixed; margin-top:210px; margin-left: -6px; display:block; z-index:9999999;}
.social_vertical a{text-decoration:none; color:#FFF; width:36px; height:36px; text-indent:-9999px; }
.social_vertical ul{ margin:0;  padding:0; background-color: #dfdfdf; width:38px;}
.social_vertical ul li{ list-style-type:none;}
.social_vertical ul li a { text-decoration:none; color:#FFF; width: 36px; height:36px; display:block; text-indent:-9999px;}
.socialbutton {clear:both;}
.socialbutton a{text-decoration:none; color:#FFF; width:34px; height:36px; display:inline-block; text-indent:-9999px; }

.social_vertical a.ang_vk, .socialbutton a.ang_vk{background:url(img/Social.png);}
.social_vertical a.ang_tw, .socialbutton a.ang_tw{background:url(img/Social.png) 0px -36px;}
.social_vertical a.ang_fb, .socialbutton a.ang_fb{background:url(img/Social.png) 0px -72px;}
.social_vertical a.ang_ok, .socialbutton a.ang_ok{background:url(img/Social.png) 0px -108px;}
.social_vertical a.ang_mm, .socialbutton a.ang_mm{background:url(img/Social.png) 0px -144px;}
.social_vertical a.ang_gp, .socialbutton a.ang_gp{background:url(img/Social.png) 0px -180px;}
.social_vertical a.ang_lj, .socialbutton a.ang_lj{background:url(img/Social.png) 0px -216px;}
 
.social_vertical a.ang_vk:hover, .socialbutton a.ang_vk:hover{background:url(img/Social.png) 36px  0px;}
.social_vertical a.ang_tw:hover, .socialbutton a.ang_tw:hover{background:url(img/Social.png) 36px -36px;}
.social_vertical a.ang_fb:hover, .socialbutton a.ang_fb:hover{background:url(img/Social.png) 36px -72px;}
.social_vertical a.ang_ok:hover, .socialbutton a.ang_ok:hover{background:url(img/Social.png) 36px -108px;}
.social_vertical a.ang_mm:hover, .socialbutton a.ang_mm:hover{background:url(img/Social.png) 36px -144px;}
.social_vertical a.ang_gp:hover, .socialbutton a.ang_gp:hover{background:url(img/Social.png) 36px -180px;}
.social_vertical a.ang_lj:hover, .socialbutton a.ang_lj:hover{background:url(img/Social.png) 36px -216px;}
</style>
<div class="sociallinks">
	<p>Поделитесь статьей </br>
	<a title="Добавить в Twitter" class="ang_tw"  href="http://twitter.com/intent/tweet?text=RT @AlexeyVLFF <?php the_title(); ?>: <?php the_permalink(); ?>" target="_blank" rel="nofollow"></a>
	<a title="Поделиться в Facebook" class="ang_fb"  href="http://facebook.com/sharer.php?url=<?php the_permalink(); ?>" target="_blank"  rel="nofollow"></a>
	<a title="Поделиться в Google +" class="ang_gp" href="http://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" rel="nofollow"></a>
	<a title="Поделиться ВКонтакте" class="ang_vk" href="http://vkontakte.ru/share.php?url=<?php the_permalink(); ?>" target="_blank" rel="nofollow"></a>
	<a title="Опубликовать в LiveJournal" class="ang_lj" href="http://www.livejournal.com/update.bml?event=<?php the_permalink(); ?>&subject=<?php the_title(); ?>" target="_blank" rel="nofollow"></a>
	</p>
</div>




<?php get_footer(); ?>