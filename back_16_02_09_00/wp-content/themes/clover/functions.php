<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 14.02.2016
 * Time: 22:06
 */

//Disabled emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

//Меню
if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}