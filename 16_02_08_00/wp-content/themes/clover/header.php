<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <title><?php wp_title('«', true, 'right'); ?></title>
    <link rel="pingback" href="clever/" />
    <?php wp_head(); ?>
    <!-- Подключаем стили-->
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/clover/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/wp-content/themes/clover/css/style.css">
    <!-- Конец: Подключаем стили-->



</head>
<body>
<header>
    <div class="header-content">
        <div class="header-content-bg">
            <img src="/wp-content/themes/clover/img/logo.png" alt = 'Частный детский сад: "Клевер"' title='Частный детский сад: "Клевер"' />
            <span class="header-phone">тел. 8 918 123-12-12</span>
            <nav class="main-navigation">
                <? wp_nav_menu(array('menu' => 'top-menu')); ?>
            </nav>
            <span  class="header-adress">Краснодар, ул. Академическая, дом 24</span>
        </div>
    </div>
</header>
<div class="content-wrapper">