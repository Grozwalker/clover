<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 14.02.2016
 * Time: 22:06
 */
 
//Подключаем шорткоды
get_template_part( 'include/shortcode');

//Подключаем хлебные крошки
get_template_part( 'include/breadcrumbs');

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);

//Disabled emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

//Меню
if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}


//Подключаем последнюю версию JQuery
function my_jquery() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
    wp_enqueue_script( 'jquery' );
}

