<?php get_header(); ?>

    <aside>
		<iframe src='/wp-content/plugins/inwidget/index.php?width=100&inline=2&view=12&toolbar=false' scrolling='no' frameborder='no' style='border:none;width:100px;height:320px;overflow:hidden;'></iframe>
    </aside>
	
	<div class = "content">


		<!--Вывод слайдера только на главной странице-->
		<?php
			if ((is_front_page()) and (!is_paged())) {
				echo '<div class="slider">';

				//Выводим слайдер только на главной странице
				if ( function_exists( 'meteor_slideshow' ) ) { meteor_slideshow(); }
				echo '</div>';
			} else {
				//<!--Вывод хлебных крошек-->
		 		if( function_exists('breadcrumbs') ) breadcrumbs();
			}
		?>

		
		<?php if (have_posts()): while (have_posts()): the_post(); ?>
			<?php the_content(); ?>
		<?php endwhile; endif; ?>
	</div>

    

<?php get_footer(); ?>