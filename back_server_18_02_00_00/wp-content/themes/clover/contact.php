<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 16.02.2016
 * Time: 23:44
 * Template Name: Contact
 */

?>


<?php get_header(); ?>





        <?php if (have_posts()): while (have_posts()): the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; endif; ?>

        <style>
            form#contact  {
                border:1px solid #e5e5e5;
                padding:10px;
                background:#e9ffd0;
                border-radius:5px;
            }
            #contact label {
                font-size: 14px;
            }
            #contact input:required:valid {
                box-shadow: 0 0 3px #BCEF89;
                border-color: #BCEF89!important;
                background: #fff url(images/valid.png) no-repeat 98% center;
            }
            #contact textarea:required:valid {
                box-shadow: 0 0 3px #BCEF89;
                border-color: #BCEF89!important;
            }
            #contact input:focus:invalid {
                box-shadow: 0 0 3px #FFDF97;
                border-color: #FFDF97!important;
                background: #fff url(images/invalid.png) no-repeat 98% center;
            }
            #contact textarea:focus:invalid {
                box-shadow: 0 0 3px #FFDF97;
                border-color: #FFDF97!important;
            }
            .err {
                border: 1px solid #ff8c00;
                padding: 10px;
                background: #FFDF97;
                text-align: left;
                border-radius: 3px;
            }
            .ok {
                border: 1px #BCEF89 solid;
                margin-bottom: 15px;
                padding: 10px;
                background: #f5f9fd;
                text-align: center;
                border-radius: 3px;
            }
            #author, #email, #url {
                width: 30%;
                padding: 5px;
                border-radius: 5px;
                border: 1px solid #e5e5e5;
            }
            #comment {
                padding: 5px;
                border-radius: 5px;
                border: 1px solid #e5e5e5;
                overflow: auto;
            }
            #submit {
                font-weight: 400;
                background: #393;
                font-size: 15px;
                color: #fff;
                padding: 10px 50px;
                border: none;
                cursor: pointer;
            }
        </style>

        <form id="contact" action="/wp-content/themes/clover/include/mail.php" method="post">
            <H3>Форма обратной связи</H3>
            <div id="note"></div>
            <div id="fields">
                <p><input type="text" name="name" id="author" placeholder="Имя" required> <label for="author">Как вас зовут</label></p>
                <p><input type="email" name="email" id="email" placeholder="E-mail" required> <label for="email">Электронная почта</label></p>
                <p><input type="text" name="sub" id="url" placeholder="Тема" required> <label for="url">Тема сообщения</label></p>
                <p><textarea name="message" cols="1" rows="10" id="comment" style="width:98%" placeholder="Введите сюда текст сообщения" required></textarea></p>
                <p><button type="submit" id="submit" class="go">Отправить сообщение</button></p>
            </div>
        </form>

        <!--Скрипт для формы обратной связи-->
        <script src="/wp-content/themes/clover/include/js/contact.js"></script>





<?php get_footer(); ?>