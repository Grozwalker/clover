<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 14.02.2016
 * Time: 22:06
 */
 
//Подключаем шорткоды
get_template_part( 'include/shortcode');

//Подключаем миниатюры
get_template_part(  'include/class.Make_Thumb');

//Подключаем хлебные крошки
get_template_part( 'include/breadcrumbs');

remove_filter( 'the_content', 'wpautop' );
add_filter( 'the_content', 'wpautop' , 12);

//Disabled emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

//Меню
if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}


//Подключаем последнюю версию JQuery
function my_jquery() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js');
    wp_enqueue_script( 'jquery' );
}

//Подключаем миниатюры страницы
if ( function_exists( 'add_theme_support' ) )
add_theme_support( 'post-thumbnails' );


//Добавляем rel к изображениям галлереи
/* 
 * Изменение вывода галереи через шоткод 
 * Смотреть функцию gallery_shortcode в http://wp-kama.ru/filecode/wp-includes/media.php
 * $output = apply_filters( 'post_gallery', '', $attr );
 */
add_filter('post_gallery', 'my_gallery_output', 10, 2);
function my_gallery_output( $output, $attr ){
	$ids_arr = explode(',', $attr['ids']);
	$ids_arr = array_map('trim', $ids_arr );

	$pictures = get_posts( array(
		'posts_per_page' => -1,
		'post__in'       => $ids_arr,
		'post_type'      => 'attachment',
		'orderby'        => 'post__in',
	) );

	if( ! $pictures ) return 'Запрос вернул пустой результат.';

	// Вывод
	$out = '<dl class="gallery_photos">';

	// Выводим каждую картинку из галереи
	foreach( $pictures as $pic ){
		$src = $pic->guid;
		$t = esc_attr( $pic->post_title );
		$title = ( $t && false === strpos($src, $t)  ) ? $t : '';

		$caption = ( $pic->post_excerpt != '' ? $pic->post_excerpt : $title );
		
		$thumbnail_attributes = wp_get_attachment_image_src( get_post_thumbnail_id($page->ID), 'medium' );

		$out .= '<dt>
			<a href="'. $src .'" class = "gallery" rel = "gallery1"><img src="'. kama_thumb_src('w=185&h=120&src='. $src ) .'" alt="'. $title .'" /></a>'. 
			( $caption ? "<span class='caption'>$caption</span>" : '' ) .
		'</dt>';
	}

	$out .= '</dl>';

	return $out;
}